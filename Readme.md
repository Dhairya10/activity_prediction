## Prerequisites

-   Linux or macOS
-   Python 3
-   NVIDIA GPU + CUDA cuDNN
-   PyTorch 0.4

## [](https://github.com/NVIDIA/vid2vid#getting-started)Getting Started

### [](https://github.com/NVIDIA/vid2vid#installation)Installation

-   Install python libraries  [dominate](https://github.com/Knio/dominate)  and requests.

``` pip install dominate requests ```


## Testing

-   Please first download example dataset by running 

 `python scripts/download_datasets.py`.
    
-   Next, compile a snapshot of  [FlowNet2](https://github.com/NVIDIA/flownet2-pytorch)  by running  

`python scripts/download_flownet2.py`.
    
-   Cityscapes
    
    -   Please download the pre-trained Cityscapes model by:

` python scripts/street/download_models.py`
        
    -   To test the model (`bash ./scripts/street/test_2048.sh`):


` python test.py --name label2city_2048 --label_nc 35 --loadSize 2048 --n_scales_spatial 3 --use_instance --fg --use_single_G
`

## Result

Image generated using a Generative Adversarial Network.
The GAN architecture was defined by Nvidia in their research paper [vid2vid](https://github.com/NVIDIA/vid2vid)

![fake_B_stuttgart_00_000000_000013_leftImg8bit](https://user-images.githubusercontent.com/36099337/67398513-68154700-f5c8-11e9-8a10-06772492f3e3.jpg)


